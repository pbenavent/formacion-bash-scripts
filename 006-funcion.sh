#!/bin/bash

escribe_en_log () {

# Fichero de log
function_log=/tmp/$(date +%Y%m%d)-$$.log
# Todas las variables recibidas por la funcion
texto=$*

	if [[ $# -le 0 ]] 
		then
			echo -e "\n\tUso: "$0" TextoParaFicheroDeLog\n"
			exit 1
		else
			echo "$texto" >> $function_log
	fi
}

escribe_en_log $*
RETVAL=$?
echo "Finalizo la funcion con salida: "$RETVAL""


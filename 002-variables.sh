#!/bin/bash

var=${1}

# Parametros que recibe el script
echo "Tu has pasado $# paramtros al script $0"

#if [ ${var} !=0 ]
if [ -z ${var} ]
	then
		echo "var ${var} es CERO"
	else
		echo "Se almacena en variable "\$var" el contenido: ${1}"
fi

echo -e "Este fichero se llama: $(basename $0)"
echo -e "La ruta en la llamada del script es: $(dirname $0)"

# Evaluar variable vacia
if [[ -z "$var" ]] 
	then echo "La variable \$var esta VACIA"
	else echo "La variable \$var tiene contenido: "$var""
fi


# Ejecutar comandos with $()
echo "Fecha en formato YYYYMMDD:" $(date +%Y%m%d)

# Parammeters with ${}
echo "\${1} = ${1}"
echo "\${2} = ${2}"

echo “The current working directory is "$PWD"”
echo 'The current working directory is $PWD'

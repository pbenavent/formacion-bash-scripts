# formacion-bash-scripts

## Comenzando
El *objeto* de este repositorio es facilitar unos scripts en Bash Shell para mostrar a usuarios sin experiencia previa en Bash Shell.

## Cómo sincronizar este repositorio

```
cd existing_repo
git remote add origin https://gitlab.com/pbenavent/formacion-bash-scripts.git
git branch -M main
git push -uf origin main
```

***

# README
Lea este documento para ampliar información sobre el respositorio

## Nombre del proyecto
Formacion-bash-scripts.

## Descripción
El objeto de los scripts en este repositorio es ilustrar la formación ***básica*** de Shell Bash. Los ejemplos son deliveradamente los más simples posibles para centrar la atención en lo explicado.

## Instalación
Los scripts requieren que se disponga de una versión de Bash Shell 4.4 o superior instalada en `/bin/bash`.

Ejecutar los scritps requiere de una de estas dos opciones:

1. Cambiar los permisos a los scripts
    `chmod 754 $NombreDelScript`
    Ejecución después de cambiar permisos:
    `./$NombreDelScript`
2. Invocarlos desde la línea de comandos así:
    `/bin/bash $NombreDelScript`

## Soporte
Los script se facilitan a título de ejemplo, sin soporte, para que el alumno los modifique o reutilice a su elección.

## Contribución
Si deseas aportar tus ejemplos contacta con el propietario del repositorio [pbenavent](https://gitlab.com/pbenavent)

- [pbenavent unmaintaned personal blog](http://benavent.org/wp/)
- [pbenavent at Mastodon](https://social.linux.pizza/@pbenavent)
- [pbenavent a LinkedIn](http://es.linkedin.com/in/pbenavent)

## Licencia
Los scripts de ejemplo se facilitan bajo licencia [GPLv3](https://www.gnu.org/licenses/gpl.html)

## Estado del proyecto
El proyecto está en permanente actualización.

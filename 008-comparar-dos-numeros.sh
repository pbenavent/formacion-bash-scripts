#!/bin/bash

# Comprobar que se han pasado 2 numeros a comparar
if [[ $# -lt 2 ]]
	then echo "Se necesitan 2 valores numericos a comparar" && exit 1
fi

# Algunas comparaciones numericas de ejemplo
# ------------------------------------------
# Menor que
if [[ $1 -lt $2 ]]
	then
		echo ""$1" es menor que "$2""
	else
		echo "NO "$1" es menor que "$2""
fi

# Menor o igual que
if [[ $1 -le $2 ]]
	then
		echo ""$1" es menor o igual que "$2""
	else
		echo "NO "$1" es menor o igual que "$2""
fi

# Mayor o igual que
if [[ $1 -ge $2 ]]
	then
		echo ""$1" es mayor o igual que "$2""
	else
		echo "NO "$1" es mayor o igual que "$2""
fi

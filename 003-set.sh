#!/bin/bash
set -e
# -e: exit si falla cualquier comando
test -d nosuchdir	# este comando falla, si lo comentamos, acaba todo el script
echo no dir 		# por -e hace exit al fallar y no se ejecuta
echo "Ha llegado al final del script"

#!/bin/bash

# La ultima marca de coches es separada como si fueran dos
cars=(Toyota Honda Renault "Maruti Suzuki")
echo "### Primer prueba MAL ###"
for car in ${cars[*]}
do
  echo $car
done
read

# Se define un nuevo separador de campo y si se trata como una marca
echo "### Primera prueba BIEN ###"
IFS=$'\n'
cars=(Toyota Honda Renault "Maruti Suzuki")
for car in ${cars[*]}
do
  echo $car
done
read

# Se define un nuevo separador de campo
echo "### Tercera prueba MAL ###"
IFS=" "
people=("fulanito@mercadona,es;menganito@mercadona.es;sotanito@mercadona.es")
for person in ${people[*]}
do
	echo $person
done
read

echo "### Tercera prueba BIEN ###"
IFS=";"
people=("fulanito@mercadona,es;menganito@mercadona.es;sotanito@mercadona.es")
for person in ${people[*]}
do
	echo $person
done
